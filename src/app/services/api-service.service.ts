import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

export interface Request {
categorie?: any,
}

@Injectable({
providedIn: 'root'
})
export class apiServiceService {

  constructor(private http: HttpClient) { }

  public getAll() {
    return this.http.get<Request[]>(environment.backUrl);
  }

  public add(entity) {
    return this.http.post<Request[]>(environment.backUrl + 'admin/add', entity);
  }

  public edit(id, entity) {
    return this.http.put<Request[]>(environment.backUrl + 'admin/edit/' + id, entity);
  }

  public remove(id) {
    return this.http.delete(environment.backUrl + 'admin/remove/' + id)
  }
}