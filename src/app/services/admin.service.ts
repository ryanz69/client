import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import * as jwtdecode from 'jwt-decode';
import { environment } from 'src/environments/environment';

export interface RequestUser {
username: string,
password: string,
}

@Injectable({
providedIn: 'root'
})
export class adminService {

  private logged = new BehaviorSubject<boolean>(false);

  expiredDate = new Date();
  token;

  get isLogged() {
    if(this.cookieService.get('token')){
      this.logged.next(true);
    }
    return this.logged.asObservable();
  }

  cookieToken(data) {
    this.expiredDate.setTime( this.token.exp * 1000 );
    this.cookieService.set( 'token', data, this.expiredDate);
  }

  constructor(private http:HttpClient, private cookieService: CookieService, private router: Router) {  }

  public login(user) {
    return this.http.post<RequestUser[]>( environment.backUrl + '/login', user)
    .pipe(map(data => {
    if(data) {
      this.token = jwtdecode(data['token']);
      this.logged.next(true);
      this.cookieToken('Bearer ' + data['token']);
      this.router.navigate(['/', 'admin']);
    }
    }));
  }

  public logout() {
      this.logged.next(false);
    this.cookieService.delete('token');
    this.router.navigate(['/', 'home']);
  }
}