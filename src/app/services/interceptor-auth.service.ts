import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import { adminService } from './admin.service';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
providedIn: 'root'
})
export class InterceptorAuthService implements HttpInterceptor {

  constructor(private cookieService: CookieService, private adminService: adminService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const idToken = this.cookieService.get('token');

    const cloned = req.clone({
      headers: req.headers.set('Authorization', idToken)
    });

    return next.handle(cloned).pipe(
      catchError(response => {
      if (response instanceof HttpErrorResponse) {
        if(response.status == 401) {
          if(req.url != environment.backUrl + '/login') {
            this.adminService.logout();
          }
        }
      }
      return throwError(response);	 
      })
    );
  }
}